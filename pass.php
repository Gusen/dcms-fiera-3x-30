<?
include_once 'sys/inc/start.php';
include_once 'sys/inc/compress.php';
include_once 'sys/inc/sess.php';
include_once 'sys/inc/home.php';
include_once 'sys/inc/settings.php';
include_once 'sys/inc/db_connect.php';
include_once 'sys/inc/ipua.php';
include_once 'sys/inc/fnc.php';
include_once 'sys/inc/shif.php';
$show_all = true; // показ для всех
include_once 'sys/inc/user.php';
lang::start('default');
only_unreg();
$set['title'] = lang('Восстановление пароля');
include_once 'sys/inc/thead.php';
title();

if (isset($_POST['nick']) && isset($_POST['mail']) && $_POST['nick']!=NULL && $_POST['mail']!=NULL)
{
if (mysql_result(mysql_query("SELECT COUNT(*) FROM `user` WHERE `nick` = '".my_esc($_POST['nick'])."' AND `no_repass` = '1'"), 0) == 1)
{
	$err = lang('Данный профиль нельзя востановить');
}		
elseif (mysql_result(mysql_query("SELECT COUNT(*) FROM `user` WHERE `nick` = '".my_esc($_POST['nick'])."'"), 0) == 0)
{
	$err = lang("Пользователь с таким логином не зарегистрирован");
}
elseif (mysql_result(mysql_query("SELECT COUNT(*) FROM `user` WHERE `nick` = '".my_esc($_POST['nick'])."' AND `ank_mail` = '".my_esc($_POST['mail'])."'"), 0) == 0)
{
	$err = lang('Неверный адрес E-mail или информация о E-mail отсутствует');
}
else 
{ 
$q = mysql_query("SELECT * FROM `user` WHERE `nick` = '".my_esc($_POST['nick'])."' LIMIT 1");
$user2 = mysql_fetch_assoc($q);
$new_sess=substr(md5(passgen()), 0, 20);
$subject = lang("Восстановление пароля");
$regmail = lang("Здравствуйте")." $user2[nick]<br />
".lang("Вы активировали восстановление пароля<br />Для установки нового пароля перейдите по ссылке:<br />")."
<a href='http://$_SERVER[HTTP_HOST]/pass.php?id=$user2[id]&amp;set_new=$new_sess'>http://$_SERVER[HTTP_HOST]/pass.php?id=$user2[id]&amp;set_new=$new_sess</a><br />
".lang("Данная ссылка действительна до первой авторизации под своим логином")." ($user2[nick])<br />
".lang("С уважением, администрация сайта")."<br/>"; 

$adds = "From: \"password@$_SERVER[HTTP_HOST]\" <password@$_SERVER[HTTP_HOST]>\n";
$adds .= "Content-Type: text/html; charset=utf-8\n";
mail($user2['ank_mail'],'=?utf-8?B?'.base64_encode($subject).'?=',$regmail,$adds);

mysql_query("UPDATE `user` SET `sess` = '$new_sess' WHERE `id` = '$user2[id]' LIMIT 1");

msg(lang("Ссылка для установки нового пароля отправлена на e-mail")." $user2[ank_mail]");
}
}
 

if (isset($_GET['id']) AND isset($_GET['set_new']) AND strlen($_GET['set_new']) == 20 AND 
mysql_result(mysql_query("SELECT COUNT(*) FROM `user` WHERE `id` = '".intval($_GET['id'])."' AND `sess` = '".my_esc($_GET['set_new'])."'"), 0) == 1)
{
$q = mysql_query("SELECT * FROM `user` WHERE `id` = '".intval($_GET['id'])."' LIMIT 1");
$user2 = mysql_fetch_assoc($q);

if (isset($_POST['pass1']) && isset($_POST['pass2']))
{
	if ($_POST['pass1']==$_POST['pass2'])
	{
		if (strlen2($_POST['pass1']) < 6)$err = lang('По соображениям безопасности новый пароль не может быть короче 6-ти символов');
		if (strlen2($_POST['pass1']) > 32)$err = lang('Длина пароля превышает 32 символа');
	}
	else
	{
		$err = lang('Новый пароль не совпадает с подтверждением');
	}
	
if (!isset($err))
{
	setcookie('id_user', $user2['id'], time()+60*60*24*365);
	mysql_query("UPDATE `user` SET `pass` = '".shif($_POST['pass1'])."' WHERE `id` = '$user2[id]' LIMIT 1");
	setcookie('pass', cookie_encrypt($_POST['pass1'],$user2['id']), time()+60*60*24*365);
	msg(lang('Пароль успешно изменен'));
}
} 

err();
aut();


echo "<form action='/pass.php?id=$user2[id]&amp;set_new=".esc($_GET['set_new'],1)."&amp;$passgen' method=\"post\">\n";
echo "Логин:<br />\n";
echo "<input type=\"text\" disabled='disabled' value='$user2[nick]' maxlength=\"32\" size=\"16\" /><br />\n";
echo "Новый пароль:<br />\n<input type='password' name='pass1' value='' /><br />\n";
echo "Подтверждение:<br />\n<input type='password' name='pass2' value='' /><br />\n";
echo "<input type='submit' name='save' value='Изменить' />\n";
echo "</form>\n";

}
else
{
err();
aut();


echo "<div class='p_m'><form action=\"?$passgen\" method=\"post\">\n";
echo lang('Логин').":<br />\n";
echo "<input type=\"text\" name=\"nick\" title=\"".lang('Логин')."\" value=\"\" maxlength=\"32\" size=\"16\" /><br />\n";
echo lang('E-mail').":<br />\n";
echo "<input type=\"text\" name=\"mail\" title=\"".lang('E-mail')."\" value=\"\" maxlength=\"32\" size=\"16\" /><br />\n";
echo "<input type=\"submit\" value=\"".lang('Далее')."\" title=\"".lang('Далее')."\" />";
echo "</form>";
echo lang('На ваш e-mail придет ссылка для установки нового пароля.')."<br />\n";
echo lang("Если у вас в анкете отсутствует запись о вашем e-mail, восстановление пароля невозможно.")."</div>";
}
 
echo"
	<div class='foot'>
	".lang("Еще не зарегистрированы?")." <br />
	<a href='/reg.php'>".lang("Регистрация")."</a><br />
	</div>
	<div class='foot'>
	".lang("Уже зарегистрированы?")." <br />
	<a href='/aut.php'>".lang("Авторизация")."</a><br />
	</div>
";

include_once 'sys/inc/tfoot.php';
?>